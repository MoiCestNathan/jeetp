<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <title>Votre Panier</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
    <style>
        table {
            width: 100%;
            border-collapse: collapse;
        }
        th, td {
            border: 1px solid black;
            padding: 8px;
            text-align: left;
        }
    </style>
</head>
<body>
    <h1>Panier</h1>
    <table>
        <thead>
            <tr>
                <th>Article</th>
                <th>Prix Unité</th>
                <th>Quantité</th>
                <th>Sous-total</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="item" items="${cart}">
                <tr>
                    <td>${item.key.name}</td>
                    <td>${item.key.price}</td>
                    <td>${item.value}</td>
                    <td>${item.key.price * item.value}</td>
                </tr>
            </c:forEach>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="3">Prix total :</th>
                <th>${totalPrice}</th>
            </tr>
        </tfoot>
    </table>
    <a href="articles">Retour aux articles</a>
</body>
</html>
