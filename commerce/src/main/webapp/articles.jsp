<%@ page import="java.util.List" %>
<%@ page import="fr.univtours.polytech.commerce.model.ArticleBean" %>
<%@ page import="fr.univtours.polytech.commerce.model.CartBean" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>Liste des articles</title>
    <style>
        table {
            width: 100%;
            border-collapse: collapse; /* Pour enlever tout espace entre les bordures */
        }
        th, td {
            border: 1px solid black; /* Rendre les bordures visibles */
            padding: 5px;
            text-align: center;
        }
        th {
            background-color: #f2f2f2;
            position: sticky;
            top: 0; /* Assure que la tête du tableau reste visible lors du défilement */
        }
        .empty-cell {
            background-color: #fff; /* Pour un cellule vide visuellement distinguée, si nécessaire */
        }
    </style>
</head>
<body>
    <div id="username-display">
        <p>Connecté en tant que : <%= session.getAttribute("login") %></p>
        <a href="logout">Déconnexion</a>
    </div>
    <h1>Liste des articles</h1>
    <table>
        <thead>
            <tr>
                <th>Nom</th>
                <th>Prix</th>
                <th>Restant</th>
                <th class="empty-cell"></th>
                <th>Enlever 1 au panier</th>
                <th>Actuellement dans votre panier</th>
                <th>Ajouter 1 au panier</th>
            </tr>
        </thead>
        <tbody>
            <%
            List<ArticleBean> articles = (List<ArticleBean>) request.getAttribute("articles");
            CartBean cart = (CartBean) request.getAttribute("cart");
            if (articles != null && !articles.isEmpty()) {
                for (ArticleBean article : articles) {
                    int quantityInCart = cart.getArticleQuantity(article.getId());
            %>
            <tr>
                <td><%= article.getName() %></td>
                <td><%= article.getPrice() %></td>
                <td><%= article.getStock() %></td>
                <td class="empty-cell"></td>
                <td>
                    <a href="updateCart?articleId=<%= article.getId() %>&action=remove">-</a>
                </td>
                <td>
                    <%= quantityInCart %>
                </td>
                <td>
                    <a href="updateCart?articleId=<%= article.getId() %>&action=add">+</a>
                </td>
            </tr>
            <%
                }
            } else {
            %>
            <tr>
                <td colspan="7">Aucun article disponible</td>
            </tr>
            <%
            }
            %>
        </tbody>
    </table>
    <form action="cart" method="get">
        <button id="cart">Voir panier</button>
    </form>
</body>
</html>
