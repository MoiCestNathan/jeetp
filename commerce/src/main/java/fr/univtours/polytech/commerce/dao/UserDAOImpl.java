package fr.univtours.polytech.commerce.dao;

import fr.univtours.polytech.commerce.model.UserBean;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Stateless
public class UserDAOImpl implements UserDAO {
    @PersistenceContext(unitName = "Commerce")
    private EntityManager em;

    @Override
    public UserBean findUserByLogin(String login) {
        try {
            return em.find(UserBean.class, login);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

