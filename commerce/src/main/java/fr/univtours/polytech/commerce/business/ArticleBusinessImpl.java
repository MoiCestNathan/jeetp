package fr.univtours.polytech.commerce.business;

import java.util.List;

import fr.univtours.polytech.commerce.dao.ArticleDAO;
import fr.univtours.polytech.commerce.model.ArticleBean;
import fr.univtours.polytech.commerce.model.CartBean;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

/**
 * Classe d'implémentation des opérations métiers liées aux articles.
 */
@Stateless
public class ArticleBusinessImpl implements ArticleBusiness {

    @EJB
    private ArticleDAO articleDAO;

    @PersistenceContext(unitName = "Commerce")
    private EntityManager em;

    /**
     * Récupère la liste de tous les articles disponibles.
     * 
     * @return la liste des articles
     */
    @Override
    public List<ArticleBean> getAllArticles() {
        return articleDAO.findAllArticles();
    }

    /**
     * Ajoute un article spécifié par son ID au panier d'achat d'un utilisateur.
     * 
     * @param articleId L'ID de l'article à ajouter
     * @param cart Le panier dans lequel ajouter l'article
     */
    public void addToCart(Long articleId, CartBean cart) {
        ArticleBean article = articleDAO.find(articleId);
        if (article != null && article.getStock() > 0) {
            article.setStock(article.getStock() - 1);
            em.merge(article); // Mettre à jour l'article
            cart.addArticle(article); // Ajouter à la session de l'utilisateur
        }
    }
    
    /**
     * Retire un article du panier d'achat.
     * 
     * @param articleId L'ID de l'article à retirer
     * @param cart Le panier duquel retirer l'article
     */
    public void removeFromCart(Long articleId, CartBean cart) {
        if (cart.containsArticle(articleId)) {
            ArticleBean article = articleDAO.find(articleId);
            if (article != null) {
                int currentQuantity = cart.getArticleQuantity(articleId);
                if (currentQuantity > 0) {
                    cart.removeArticle(articleId); // Retirer de la session de l'utilisateur
                    article.setStock(article.getStock() + 1); // Augmenter le stock
                    em.merge(article); // Mettre à jour l'article
                }
            }
        }
    }

    /**
     * Remet en stock une certaine quantité d'un article donné.
     * 
     * @param articleId L'ID de l'article
     * @param quantity La quantité de l'article à remettre en stock
     */
    public void returnItemsToStock(Long articleId, Integer quantity) {
        ArticleBean article = articleDAO.find(articleId);
        if (article != null) {
            article.setStock(article.getStock() + quantity);
            em.merge(article);
        }
    }
    
    /**
     * Recherche un article par son ID et le retourne.
     * 
     * @param id L'ID de l'article à rechercher
     * @return L'article trouvé ou null si aucun article n'est trouvé
     */
    public ArticleBean findArticleById(Long id) {
        ArticleBean article = em.find(ArticleBean.class, id);
        System.out.println("Finding article by ID: " + id + ", Result: " + article);
        return article;
    }

}
