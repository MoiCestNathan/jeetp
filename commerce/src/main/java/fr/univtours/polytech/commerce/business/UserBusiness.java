package fr.univtours.polytech.commerce.business;

import jakarta.ejb.Local;

/**
 * Interface locale définissant les opérations métiers liées à la gestion des utilisateurs.
 */
@Local
public interface UserBusiness {

    /**
     * Authentifie un utilisateur à partir de son login et mot de passe.
     * 
     * @param login Le login de l'utilisateur.
     * @param password Le mot de passe de l'utilisateur.
     * @return true si l'authentification est réussie, false autrement.
     */
    boolean authenticate(String login, String password);
}
