package fr.univtours.polytech.commerce.dao;

import java.util.List;

import fr.univtours.polytech.commerce.model.ArticleBean;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

@Stateless
public class ArticleDAOImpl implements ArticleDAO {
    @PersistenceContext(unitName = "Commerce")
    private EntityManager em;

    @Override
    public List<ArticleBean> findAllArticles() {
        List<ArticleBean> articles = em.createQuery("SELECT a FROM ArticleBean a", ArticleBean.class).getResultList();
        System.out.println("Articles retrieved from database: " + articles);
        return articles;    
    }

    public ArticleBean find(Long id) {
        return em.find(ArticleBean.class, id);
    }
    
}
