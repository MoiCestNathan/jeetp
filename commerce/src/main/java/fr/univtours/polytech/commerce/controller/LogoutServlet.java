package fr.univtours.polytech.commerce.controller;

import java.io.IOException;
import java.util.Map;

import fr.univtours.polytech.commerce.business.ArticleBusiness;
import fr.univtours.polytech.commerce.model.CartBean;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(name = "logoutServlet", urlPatterns = { "/logout" })
public class LogoutServlet extends HttpServlet {

    @EJB
    private ArticleBusiness articleBusiness;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        CartBean cart = (CartBean) session.getAttribute("cart");

        if (cart != null) {
            Map<Long, Integer> articlesInCart = cart.getArticles();
            for (Map.Entry<Long, Integer> entry : articlesInCart.entrySet()) {
                Long articleId = entry.getKey();
                Integer quantity = entry.getValue();
                articleBusiness.returnItemsToStock(articleId, quantity);
            }
        }

        session.invalidate();

        response.sendRedirect("login.jsp");
    }
}