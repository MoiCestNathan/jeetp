package fr.univtours.polytech.commerce.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.univtours.polytech.commerce.business.ArticleBusiness;
import fr.univtours.polytech.commerce.model.ArticleBean;
import fr.univtours.polytech.commerce.model.CartBean;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/showCart")
public class ShowCartServlet extends HttpServlet {
    @EJB
    private ArticleBusiness articleBusiness;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        CartBean cart = (CartBean) session.getAttribute("cart");
        if (cart == null) {
            cart = new CartBean();
            session.setAttribute("cart", cart);
        }

        System.out.println("CartBean retrieved from session: " + cart);

        double totalPrice = 0;
        Map<ArticleBean, Integer> cartDetails = new HashMap<>();
        for (Map.Entry<Long, Integer> entry : cart.getArticles().entrySet()) {
            ArticleBean article = articleBusiness.findArticleById(entry.getKey());

            System.out.println("Article retrieved: " + article);

            if (article != null) {
                int quantity = entry.getValue();
                totalPrice += article.getPrice() * quantity;
                cartDetails.put(article, quantity);
            }
        }

        System.out.println("Total price calculated: " + totalPrice);
        request.setAttribute("totalPrice", totalPrice);
        request.setAttribute("cart", cartDetails.entrySet());
        request.getRequestDispatcher("/cart.jsp").forward(request, response);
    }
}


