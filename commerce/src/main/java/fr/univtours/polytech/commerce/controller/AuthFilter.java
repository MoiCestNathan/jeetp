package fr.univtours.polytech.commerce.controller;

import java.io.IOException;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebFilter("/*")  // Appliquer le filtre à toutes les requêtes
public class AuthFilter implements Filter {

    public void init(FilterConfig filterConfig) throws ServletException {
        // Configuration initiale du filtre, si nécessaire
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpSession session = httpRequest.getSession(false);  // Ne pas créer de session si elle n'existe pas

        String loginURI = httpRequest.getContextPath() + "/login.jsp";
        String loginProcessingURI = httpRequest.getContextPath() + "/login";  // URL de traitement du login

        boolean loggedIn = session != null && session.getAttribute("user") != null;
        boolean loginRequest = httpRequest.getRequestURI().equals(loginURI);
        boolean loginProcessingRequest = httpRequest.getRequestURI().equals(loginProcessingURI);

        // Exclure également les fichiers statiques (par exemple, .js, .css, images, etc.)
        boolean isStaticResource = httpRequest.getRequestURI().matches(".*\\.(css|jpg|png|gif|js)");

        if (loggedIn || loginRequest || loginProcessingRequest || isStaticResource) {
            chain.doFilter(request, response);  // Laissez la requête continuer normalement.
        } else {
            httpResponse.sendRedirect(loginURI);  // Redirigez vers la page de connexion
        }
    }

    public void destroy() {
        // Nettoyage avant que le filtre ne soit détruit, si nécessaire
    }
}

