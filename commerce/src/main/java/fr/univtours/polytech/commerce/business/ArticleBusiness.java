package fr.univtours.polytech.commerce.business;

import java.util.List;
import fr.univtours.polytech.commerce.model.ArticleBean;
import fr.univtours.polytech.commerce.model.CartBean;
import jakarta.ejb.Local;

/**
 * Interface définissant les opérations métiers pour la gestion des articles dans une plateforme de commerce en ligne.
 */
@Local
public interface ArticleBusiness {

    /**
     * Récupère tous les articles disponibles dans le dépôt.
     *
     * @return une liste de toutes les instances de {@link ArticleBean}
     */
    List<ArticleBean> getAllArticles();

    /**
     * Ajoute un article au panier.
     *
     * @param articleId l'identifiant de l'article à ajouter
     * @param cart le {@link CartBean} panier dans lequel l'article doit être ajouté
     */
    void addToCart(Long articleId, CartBean cart);

    /**
     * Retire un article du panier.
     *
     * @param articleId l'identifiant de l'article à retirer
     * @param cart le {@link CartBean} panier duquel l'article doit être retiré
     */
    void removeFromCart(Long articleId, CartBean cart);

    /**
     * Retourne des articles au stock depuis le panier de l'utilisateur en cas d'annulation ou de retour.
     *
     * @param articleId l'identifiant de l'article à retourner
     * @param quantity la quantité à retourner au stock
     */
    void returnItemsToStock(Long articleId, Integer quantity);

    /**
     * Trouve un article par son identifiant.
     *
     * @param id l'identifiant de l'article à trouver
     * @return l'article trouvé ou {@code null} si aucun article n'est trouvé
     */
    ArticleBean findArticleById(Long id);
}
