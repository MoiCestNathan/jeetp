package fr.univtours.polytech.commerce.controller;

import java.io.IOException;
import java.util.List;

import fr.univtours.polytech.commerce.business.ArticleBusiness;
import fr.univtours.polytech.commerce.model.ArticleBean;
import fr.univtours.polytech.commerce.model.CartBean;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * Servlet dédié à la gestion des requêtes pour afficher les articles disponibles.
 */
@WebServlet("/articles")
public class ArticleServlet extends HttpServlet {
    @EJB
    private ArticleBusiness articleBusiness;

    /**
     * Traite les requêtes GET pour récupérer tous les articles disponibles et les afficher.
     * 
     * @param request La requête HTTP entrante.
     * @param response La réponse HTTP à envoyer.
     * @throws ServletException Si une exception de servlet se produit.
     * @throws IOException Si une exception d'entrée/sortie se produit.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Récupère la liste des articles disponibles
        List<ArticleBean> articles = articleBusiness.getAllArticles();

        // Gestion du panier dans la session utilisateur
        HttpSession session = request.getSession();
        CartBean cart = (CartBean) session.getAttribute("cart");
    
        // Crée un nouveau panier si aucun n'existe
        if (cart == null) {
            cart = new CartBean();
            session.setAttribute("cart", cart);
        }

        // Récupère l'utilisateur connecté s'il existe
        String login = (String) session.getAttribute("user");

        // Log pour débogage
        System.out.println("login " + login);
        System.out.println("Sending articles to JSP: " + articles);

        // Envoie des données à la JSP
        request.setAttribute("articles", articles);
        request.setAttribute("cart", cart);
        request.getRequestDispatcher("/articles.jsp").forward(request, response);
    }
}
