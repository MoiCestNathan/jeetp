package fr.univtours.polytech.commerce.business;

import fr.univtours.polytech.commerce.dao.UserDAO;
import fr.univtours.polytech.commerce.model.UserBean;
import jakarta.ejb.EJB;
import jakarta.ejb.Stateless;

/**
 * Classe Stateless EJB fournissant les fonctionnalités métiers pour les opérations liées aux utilisateurs.
 */
@Stateless
public class UserBusinessImpl implements UserBusiness {

    @EJB
    private UserDAO userDAO;

    /**
     * Authentifie un utilisateur en vérifiant si le login et le mot de passe correspondent à ceux stockés en base de données.
     * 
     * @param login Le nom d'utilisateur utilisé pour se connecter.
     * @param password Le mot de passe correspondant au nom d'utilisateur.
     * @return boolean True si les informations d'identification sont valides, False autrement.
     */
    @Override
    public boolean authenticate(String login, String password) {
        UserBean user = userDAO.findUserByLogin(login);
        return user != null && user.getPassword().equals(password);
    }
}
