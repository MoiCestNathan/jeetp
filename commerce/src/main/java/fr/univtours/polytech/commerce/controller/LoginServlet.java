package fr.univtours.polytech.commerce.controller;

import java.io.IOException;

import fr.univtours.polytech.commerce.business.UserBusiness;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    @EJB
    private UserBusiness userBusiness;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if (userBusiness.authenticate(login, password)) {
            request.getSession().setAttribute("user", login);
            response.sendRedirect("articles");
            System.out.println("Conx done");
        } else {
            request.setAttribute("errorMessage", "Le nom d'utilisateur ou le mot de passe saisit n'est pas valide.");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
}
