package fr.univtours.polytech.commerce.dao;

import java.util.List;

import fr.univtours.polytech.commerce.model.ArticleBean;
import jakarta.ejb.Local;

@Local
public interface ArticleDAO {
    List<ArticleBean> findAllArticles();
    public ArticleBean find(Long id);
}
