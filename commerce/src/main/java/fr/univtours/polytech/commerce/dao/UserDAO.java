package fr.univtours.polytech.commerce.dao;

import fr.univtours.polytech.commerce.model.UserBean;
import jakarta.ejb.Local;

@Local
public interface UserDAO {
    UserBean findUserByLogin(String login);
}

