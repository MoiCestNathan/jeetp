package fr.univtours.polytech.commerce.controller;

import java.io.IOException;

import fr.univtours.polytech.commerce.business.ArticleBusiness;
import fr.univtours.polytech.commerce.model.CartBean;
import jakarta.ejb.EJB;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/updateCart")
public class UpdateCartServlet extends HttpServlet {
    @EJB
    private ArticleBusiness articleBusiness;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        Long articleId = Long.valueOf(request.getParameter("articleId"));
        HttpSession session = request.getSession();
        CartBean cart = (CartBean) session.getAttribute("cart");
        
        if (cart == null) {
            cart = new CartBean();
            session.setAttribute("cart", cart);
        }
        
        if ("add".equals(action)) {
            // Vérifier la disponibilité de l'article et l'ajouter au panier
            articleBusiness.addToCart(articleId, cart);
        } else if ("remove".equals(action)) {
            // Enlever l'article du panier
            articleBusiness.removeFromCart(articleId, cart);
        }
        
        response.sendRedirect("articles");
    }
}

