package fr.univtours.polytech.commerce.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class CartBean implements Serializable {
    private Map<Long, Integer> articles = new HashMap<>();
    
    public void addArticle(ArticleBean article) {
        Integer quantity = articles.getOrDefault(article.getId(), 0);
        articles.put(article.getId(), quantity + 1);
    }

    public void removeArticle(Long articleId) {
        if (articles.containsKey(articleId) && articles.get(articleId) > 0) {
            articles.put(articleId, articles.get(articleId) - 1);
        }
    }

    public boolean containsArticle(Long articleId) {
        return articles.containsKey(articleId) && articles.get(articleId) > 0;
    }

    public int getArticleQuantity(Long articleId) {
        return articles.getOrDefault(articleId, 0);
    }

    public Map<Long, Integer> getArticles() {
        return this.articles;
    }
}
