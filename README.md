# Projet de Commerce en Ligne

Ce projet est une application web simple pour gérer un site de commerce en ligne. Elle permet aux utilisateurs de se connecter, de voir une liste d'articles, de les ajouter ou de les retirer de leur panier, et de consulter le contenu de leur panier.

## Fonctionnalités

- **Connexion Utilisateur** : Les utilisateurs peuvent se connecter à l'application en utilisant leur identifiant et mot de passe.
- **Liste d'Articles** : Affiche tous les articles disponibles avec la possibilité d'ajouter ou de retirer des articles du panier.
- **Gestion du Panier** : Les utilisateurs peuvent voir les articles qu'ils ont ajoutés à leur panier et voir le total de leur commande.

## Règles de Gestion Implémentées

- [x] **RG1** : L'application vérifie que l'utilisateur est présent dans la table USER et qu'il a saisi le bon mot de passe.
  - [ ] **Bonus** : Les mots de passe ne sont pas stockés en clair en base de données. Utilisation de l'algorithme MD5 pour le hashage des mots de passe.
- [x] **RG2** : Si une URL quelconque est saisie par un utilisateur qui n'est pas connecté, il est automatiquement redirigé vers la page de connexion.
- [x] **RG3** : Tous les articles présents en base de données dans la table ARTICLE sont affichés sous la forme d'une liste.
- [x] **RG4** : Ajout des articles au panier selon la disponibilité et le nombre d'exemplaires.
- [x] **RG5** : Retrait des articles du panier selon la disponibilité et le nombre d'exemplaires.
- [ ] **RG6** : Affichage et gestion du contenu du panier, incluant les mises à jour de quantité et le calcul du total.
- [x] **RG7** : Bouton de déconnexion qui clore la session de l'utilisateur et vide le panier.
- [x] **RG8** : Affichage du nom de l'utilisateur connecté à côté du bouton de déconnexion.


## Explications

Les règles de gestions cochées ci-dessus sont fonctionnelles. 
La règle de gestion (RG6) concernant l'affichage du contenu du panier ainsi que le calcul du prix total n'est pas fonctionnel.  
